﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;
using System.IO;

public class MenuInGame : MonoBehaviour
{
    [Header("UI Settings")]
    public GameObject MainCanvas;
    public GameObject OptionsUI;
    public Toggle fullscreenToggle;
    public Slider musicVolumeSlider;
    public AudioSource MapAudio;
    public Button applyButton;
    public GameSettings gameSettings;

    // Scene
    Scene m_Scene;

    void OnEnable()
    {
        // load configs
        gameSettings = new GameSettings();

        // set all UI functions
        fullscreenToggle.onValueChanged.AddListener(delegate { OnFullscreenToggle(); });
        musicVolumeSlider.onValueChanged.AddListener(delegate { OnMusicVolumeChange(); });
        applyButton.onClick.AddListener(delegate { OnApplyButtonClick(); });

        // load saved settings
        LoadSettings();
    }

    void Start()
    {
        // get current scene
        m_Scene = SceneManager.GetActiveScene();

        // check if is main menu scene
        if (m_Scene.buildIndex == 0)
            return;

        // add options button listener
        Button btnOptions = MainCanvas.gameObject.transform.Find("InGameMenuPanel").transform.Find("SettingsBtn").gameObject.GetComponent<Button>();
        btnOptions.onClick.AddListener(Options);

        // add close options button listener
        Button btnCloseOptions = MainCanvas.gameObject.transform.Find("OptionsUI").transform.Find("CloseOptionsBtn").gameObject.GetComponent<Button>();
        btnCloseOptions.onClick.AddListener(CloseOptions);

        // add play again button listener
        Button btnPlayAgain = MainCanvas.gameObject.transform.Find("InGameMenuPanel").transform.Find("PlayAgainBtn").gameObject.GetComponent<Button>();
        btnPlayAgain.onClick.AddListener(PlayAgain);

        // add quit button listener
        Button btnQuitGame = MainCanvas.gameObject.transform.Find("InGameMenuPanel").transform.Find("QuitBtn").gameObject.GetComponent<Button>();
        btnQuitGame.onClick.AddListener(QuitGame);

        // add continue button listener
        Button btnContinue = MainCanvas.gameObject.transform.Find("InGameMenuPanel").transform.Find("ContinueBtn").gameObject.GetComponent<Button>();
        btnContinue.onClick.AddListener(Continue);
    }

    private void Update()
    {
        // get current scene
        m_Scene = SceneManager.GetActiveScene();

        // check if is main menu scene
        if (m_Scene.buildIndex == 0)
            return;

        // if press escape on keyboard and your health is > 0 pause game and show in game menu
        if (Input.GetKeyDown(KeyCode.Escape) && this.gameObject.GetComponent<PlayerBehaviour>().health > 0)
        {
            MainCanvas.gameObject.transform.Find("InGameMenuPanel").gameObject.active = !MainCanvas.gameObject.transform.Find("InGameMenuPanel").gameObject.active;
            this.gameObject.GetComponent<PlayerBehaviour>().paused = !this.gameObject.GetComponent<PlayerBehaviour>().paused;
        }

        // if game is paused block first person controller and enable mouse cursor
        if (this.gameObject.GetComponent<PlayerBehaviour>().paused)
        {
            this.gameObject.GetComponent<FirstPersonController>().enabled = false;
            Cursor.lockState = CursorLockMode.None;
            Cursor.lockState = CursorLockMode.Confined;
            Cursor.visible = true;
        }
        // active again first person controller and disable mouse cursor
        else
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.lockState = CursorLockMode.Confined;
            Cursor.visible = false;
            this.gameObject.GetComponent<FirstPersonController>().enabled = true;
        }         
    }

    public void Options()
    {
        // check if is main menu scene
        if (m_Scene.buildIndex == 0)
        {
            // show options UI    
            MainCanvas.gameObject.transform.Find("InGameMenuPanel").gameObject.SetActive(false);
            OptionsUI.SetActive(true);
        }
        else
        {
            // show options UI   and disable player behaviour  
            this.gameObject.GetComponent<PlayerBehaviour>().paused = true;
            MainCanvas.gameObject.transform.Find("InGameMenuPanel").gameObject.SetActive(false);
            OptionsUI.SetActive(true);
        }   
    }

    public void CloseOptions()
    {
        // close options UI
        MainCanvas.gameObject.transform.Find("InGameMenuPanel").gameObject.SetActive(true);
        OptionsUI.SetActive(false);
    }

    public void StartGame()
    {
        // load main game scene
        SceneManager.LoadScene(1);
    }

    public void PlayAgain()
    {
        // reload menu scene
        SceneManager.LoadScene(0);
    }

    public void Continue()
    {
        // continue game and disable all panels
        this.gameObject.GetComponent<PlayerBehaviour>().paused = false;
        MainCanvas.gameObject.transform.Find("InGameMenuPanel").gameObject.SetActive(false);
    }

    public void QuitGame()
    {
        // quit game
        Application.Quit();
        UnityEditor.EditorApplication.isPlaying = false;
    }

    // UI event
    void OnFullscreenToggle()
    {
       gameSettings.fullscren = Screen.fullScreen = fullscreenToggle.isOn;
    }

    // UI event
    void OnMusicVolumeChange()
    {
        MapAudio.volume = musicVolumeSlider.value;
        gameSettings.musicVolume = MapAudio.volume;
    }

    void SaveSettings()
    {
        // save game settings to json file
        string jsonData = JsonUtility.ToJson(gameSettings, true);
        File.WriteAllText("Assets/Resources/gamesettings.json", jsonData);
        OptionsUI.SetActive(false);
    }

    void LoadSettings()
    {
        // load game settings from json file
        gameSettings = JsonUtility.FromJson<GameSettings>(File.ReadAllText("Assets/Resources/gamesettings.json"));
        musicVolumeSlider.value = gameSettings.musicVolume;
        fullscreenToggle.isOn = gameSettings.fullscren;
        Screen.fullScreen = gameSettings.fullscren;
    }

    void OnApplyButtonClick()
    {
        // apply settings
        SaveSettings();
        CloseOptions();
    }
}


